gdjs.New_32sceneCode = {};
gdjs.New_32sceneCode.GDbackObjects1= [];
gdjs.New_32sceneCode.GDbackObjects2= [];
gdjs.New_32sceneCode.GDvibrationlinkObjects1= [];
gdjs.New_32sceneCode.GDvibrationlinkObjects2= [];
gdjs.New_32sceneCode.GDupdownlinkObjects1= [];
gdjs.New_32sceneCode.GDupdownlinkObjects2= [];
gdjs.New_32sceneCode.GDtextlinkObjects1= [];
gdjs.New_32sceneCode.GDtextlinkObjects2= [];
gdjs.New_32sceneCode.GDvollinkObjects1= [];
gdjs.New_32sceneCode.GDvollinkObjects2= [];
gdjs.New_32sceneCode.GDmovelinkObjects1= [];
gdjs.New_32sceneCode.GDmovelinkObjects2= [];
gdjs.New_32sceneCode.GDlonglinkObjects1= [];
gdjs.New_32sceneCode.GDlonglinkObjects2= [];
gdjs.New_32sceneCode.GDtimelinkObjects1= [];
gdjs.New_32sceneCode.GDtimelinkObjects2= [];
gdjs.New_32sceneCode.GDbacklinkObjects1= [];
gdjs.New_32sceneCode.GDbacklinkObjects2= [];
gdjs.New_32sceneCode.GDwifilinkObjects1= [];
gdjs.New_32sceneCode.GDwifilinkObjects2= [];
gdjs.New_32sceneCode.GDsunlinkObjects1= [];
gdjs.New_32sceneCode.GDsunlinkObjects2= [];
gdjs.New_32sceneCode.GDinfolinkObjects1= [];
gdjs.New_32sceneCode.GDinfolinkObjects2= [];
gdjs.New_32sceneCode.GDinfoObjects1= [];
gdjs.New_32sceneCode.GDinfoObjects2= [];
gdjs.New_32sceneCode.GDmiclinkObjects1= [];
gdjs.New_32sceneCode.GDmiclinkObjects2= [];

gdjs.New_32sceneCode.conditionTrue_0 = {val:false};
gdjs.New_32sceneCode.condition0IsTrue_0 = {val:false};
gdjs.New_32sceneCode.condition1IsTrue_0 = {val:false};
gdjs.New_32sceneCode.condition2IsTrue_0 = {val:false};
gdjs.New_32sceneCode.conditionTrue_1 = {val:false};
gdjs.New_32sceneCode.condition0IsTrue_1 = {val:false};
gdjs.New_32sceneCode.condition1IsTrue_1 = {val:false};
gdjs.New_32sceneCode.condition2IsTrue_1 = {val:false};
gdjs.New_32sceneCode.conditionTrue_2 = {val:false};
gdjs.New_32sceneCode.condition0IsTrue_2 = {val:false};
gdjs.New_32sceneCode.condition1IsTrue_2 = {val:false};
gdjs.New_32sceneCode.condition2IsTrue_2 = {val:false};


gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDinfoObjects1Objects = Hashtable.newFrom({"info": gdjs.New_32sceneCode.GDinfoObjects1});gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDvibrationlinkObjects1Objects = Hashtable.newFrom({"vibrationlink": gdjs.New_32sceneCode.GDvibrationlinkObjects1});gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDupdownlinkObjects1Objects = Hashtable.newFrom({"updownlink": gdjs.New_32sceneCode.GDupdownlinkObjects1});gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDtextlinkObjects1Objects = Hashtable.newFrom({"textlink": gdjs.New_32sceneCode.GDtextlinkObjects1});gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDvollinkObjects1Objects = Hashtable.newFrom({"vollink": gdjs.New_32sceneCode.GDvollinkObjects1});gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDmovelinkObjects1Objects = Hashtable.newFrom({"movelink": gdjs.New_32sceneCode.GDmovelinkObjects1});gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDlonglinkObjects1Objects = Hashtable.newFrom({"longlink": gdjs.New_32sceneCode.GDlonglinkObjects1});gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDtimelinkObjects1Objects = Hashtable.newFrom({"timelink": gdjs.New_32sceneCode.GDtimelinkObjects1});gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDbacklinkObjects1Objects = Hashtable.newFrom({"backlink": gdjs.New_32sceneCode.GDbacklinkObjects1});gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDwifilinkObjects1Objects = Hashtable.newFrom({"wifilink": gdjs.New_32sceneCode.GDwifilinkObjects1});gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDsunlinkObjects1Objects = Hashtable.newFrom({"sunlink": gdjs.New_32sceneCode.GDsunlinkObjects1});gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDmiclinkObjects1Objects = Hashtable.newFrom({"miclink": gdjs.New_32sceneCode.GDmiclinkObjects1});gdjs.New_32sceneCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("info"), gdjs.New_32sceneCode.GDinfoObjects1);

gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
gdjs.New_32sceneCode.condition1IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDinfoObjects1Objects, runtimeScene, true, false);
}if ( gdjs.New_32sceneCode.condition0IsTrue_0.val ) {
{
gdjs.New_32sceneCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.New_32sceneCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "info", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("vibrationlink"), gdjs.New_32sceneCode.GDvibrationlinkObjects1);

gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDvibrationlinkObjects1Objects, runtimeScene, true, false);
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "shakelevel", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("updownlink"), gdjs.New_32sceneCode.GDupdownlinkObjects1);

gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDupdownlinkObjects1Objects, runtimeScene, true, false);
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "landscapelevel", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("textlink"), gdjs.New_32sceneCode.GDtextlinkObjects1);

gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDtextlinkObjects1Objects, runtimeScene, true, false);
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "textlevel", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("vollink"), gdjs.New_32sceneCode.GDvollinkObjects1);

gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDvollinkObjects1Objects, runtimeScene, true, false);
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "soundlevel", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("movelink"), gdjs.New_32sceneCode.GDmovelinkObjects1);

gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDmovelinkObjects1Objects, runtimeScene, true, false);
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "movelevel", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("longlink"), gdjs.New_32sceneCode.GDlonglinkObjects1);

gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDlonglinkObjects1Objects, runtimeScene, true, false);
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "longlevel", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("timelink"), gdjs.New_32sceneCode.GDtimelinkObjects1);

gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDtimelinkObjects1Objects, runtimeScene, true, false);
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "timelevel", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("backlink"), gdjs.New_32sceneCode.GDbacklinkObjects1);

gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDbacklinkObjects1Objects, runtimeScene, true, false);
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "backlevel", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("wifilink"), gdjs.New_32sceneCode.GDwifilinkObjects1);

gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDwifilinkObjects1Objects, runtimeScene, true, false);
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "netlevel", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sunlink"), gdjs.New_32sceneCode.GDsunlinkObjects1);

gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDsunlinkObjects1Objects, runtimeScene, true, false);
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "starlevel", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("miclink"), gdjs.New_32sceneCode.GDmiclinkObjects1);

gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.New_32sceneCode.mapOfGDgdjs_46New_9532sceneCode_46GDmiclinkObjects1Objects, runtimeScene, true, false);
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "microlevel", false);
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
{gdjs.New_32sceneCode.conditionTrue_1 = gdjs.New_32sceneCode.condition0IsTrue_0;
gdjs.New_32sceneCode.condition0IsTrue_1.val = false;
gdjs.New_32sceneCode.condition1IsTrue_1.val = false;
{
{gdjs.New_32sceneCode.conditionTrue_2 = gdjs.New_32sceneCode.condition0IsTrue_1;
gdjs.New_32sceneCode.conditionTrue_2.val = (gdjs.evtTools.runtimeScene.getTime(runtimeScene, "hour") > 22);
}
}if ( gdjs.New_32sceneCode.condition0IsTrue_1.val ) {
{
{gdjs.New_32sceneCode.conditionTrue_2 = gdjs.New_32sceneCode.condition1IsTrue_1;
gdjs.New_32sceneCode.conditionTrue_2.val = (gdjs.evtTools.runtimeScene.getTime(runtimeScene, "hour") < 7);
}
}}
gdjs.New_32sceneCode.conditionTrue_1.val = true && gdjs.New_32sceneCode.condition0IsTrue_1.val && gdjs.New_32sceneCode.condition1IsTrue_1.val;
}
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
{gdjs.New_32sceneCode.conditionTrue_1 = gdjs.New_32sceneCode.condition0IsTrue_0;
gdjs.New_32sceneCode.condition0IsTrue_1.val = false;
gdjs.New_32sceneCode.condition1IsTrue_1.val = false;
{
{gdjs.New_32sceneCode.conditionTrue_2 = gdjs.New_32sceneCode.condition0IsTrue_1;
gdjs.New_32sceneCode.conditionTrue_2.val = (gdjs.evtTools.runtimeScene.getTime(runtimeScene, "hour") > 7);
}
}if ( gdjs.New_32sceneCode.condition0IsTrue_1.val ) {
{
{gdjs.New_32sceneCode.conditionTrue_2 = gdjs.New_32sceneCode.condition1IsTrue_1;
gdjs.New_32sceneCode.conditionTrue_2.val = (gdjs.evtTools.runtimeScene.getTime(runtimeScene, "hour") < 22);
}
}}
gdjs.New_32sceneCode.conditionTrue_1.val = true && gdjs.New_32sceneCode.condition0IsTrue_1.val && gdjs.New_32sceneCode.condition1IsTrue_1.val;
}
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(0);
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "255;255;255");
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "0;0;0");
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
{gdjs.deviceSensors.motion.deactivateMotionSensor();
}{gdjs.deviceSensors.orientation.deactivateOrientationSensor();
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 1;
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("vibrationlink"), gdjs.New_32sceneCode.GDvibrationlinkObjects1);
{for(var i = 0, len = gdjs.New_32sceneCode.GDvibrationlinkObjects1.length ;i < len;++i) {
    gdjs.New_32sceneCode.GDvibrationlinkObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("updownlink"), gdjs.New_32sceneCode.GDupdownlinkObjects1);
{for(var i = 0, len = gdjs.New_32sceneCode.GDupdownlinkObjects1.length ;i < len;++i) {
    gdjs.New_32sceneCode.GDupdownlinkObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("textlink"), gdjs.New_32sceneCode.GDtextlinkObjects1);
{for(var i = 0, len = gdjs.New_32sceneCode.GDtextlinkObjects1.length ;i < len;++i) {
    gdjs.New_32sceneCode.GDtextlinkObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(4)) == 1;
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("vollink"), gdjs.New_32sceneCode.GDvollinkObjects1);
{for(var i = 0, len = gdjs.New_32sceneCode.GDvollinkObjects1.length ;i < len;++i) {
    gdjs.New_32sceneCode.GDvollinkObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 1;
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("movelink"), gdjs.New_32sceneCode.GDmovelinkObjects1);
{for(var i = 0, len = gdjs.New_32sceneCode.GDmovelinkObjects1.length ;i < len;++i) {
    gdjs.New_32sceneCode.GDmovelinkObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(6)) == 1;
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("longlink"), gdjs.New_32sceneCode.GDlonglinkObjects1);
{for(var i = 0, len = gdjs.New_32sceneCode.GDlonglinkObjects1.length ;i < len;++i) {
    gdjs.New_32sceneCode.GDlonglinkObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(7)) == 1;
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("timelink"), gdjs.New_32sceneCode.GDtimelinkObjects1);
{for(var i = 0, len = gdjs.New_32sceneCode.GDtimelinkObjects1.length ;i < len;++i) {
    gdjs.New_32sceneCode.GDtimelinkObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(8)) == 1;
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("backlink"), gdjs.New_32sceneCode.GDbacklinkObjects1);
{for(var i = 0, len = gdjs.New_32sceneCode.GDbacklinkObjects1.length ;i < len;++i) {
    gdjs.New_32sceneCode.GDbacklinkObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(9)) == 1;
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("wifilink"), gdjs.New_32sceneCode.GDwifilinkObjects1);
{for(var i = 0, len = gdjs.New_32sceneCode.GDwifilinkObjects1.length ;i < len;++i) {
    gdjs.New_32sceneCode.GDwifilinkObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(10)) == 1;
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("sunlink"), gdjs.New_32sceneCode.GDsunlinkObjects1);
{for(var i = 0, len = gdjs.New_32sceneCode.GDsunlinkObjects1.length ;i < len;++i) {
    gdjs.New_32sceneCode.GDsunlinkObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(11)) == 1;
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("infolink"), gdjs.New_32sceneCode.GDinfolinkObjects1);
{for(var i = 0, len = gdjs.New_32sceneCode.GDinfolinkObjects1.length ;i < len;++i) {
    gdjs.New_32sceneCode.GDinfolinkObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(12)) == 1;
}if (gdjs.New_32sceneCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("miclink"), gdjs.New_32sceneCode.GDmiclinkObjects1);
{for(var i = 0, len = gdjs.New_32sceneCode.GDmiclinkObjects1.length ;i < len;++i) {
    gdjs.New_32sceneCode.GDmiclinkObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.New_32sceneCode.condition0IsTrue_0.val = false;
gdjs.New_32sceneCode.condition1IsTrue_0.val = false;
{
{gdjs.New_32sceneCode.conditionTrue_1 = gdjs.New_32sceneCode.condition0IsTrue_0;
gdjs.New_32sceneCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8558780);
}
}if ( gdjs.New_32sceneCode.condition0IsTrue_0.val ) {
{
{gdjs.New_32sceneCode.conditionTrue_1 = gdjs.New_32sceneCode.condition1IsTrue_0;
gdjs.New_32sceneCode.condition0IsTrue_1.val = false;
gdjs.New_32sceneCode.condition1IsTrue_1.val = false;
{
gdjs.New_32sceneCode.condition0IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(13)) == 0;
}if ( gdjs.New_32sceneCode.condition0IsTrue_1.val ) {
{
gdjs.New_32sceneCode.condition1IsTrue_1.val = !(gdjs.evtTools.systemInfo.isMobile());
}}
gdjs.New_32sceneCode.conditionTrue_1.val = true && gdjs.New_32sceneCode.condition0IsTrue_1.val && gdjs.New_32sceneCode.condition1IsTrue_1.val;
}
}}
if (gdjs.New_32sceneCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene2", true);
}}

}


{


{
{gdjs.evtsExt__DragCameraWithPointer__DragCameraWithPointer.func(runtimeScene, 0, "", "horizontal", (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}}

}


{


{
}

}


{


{
}

}


{


{
}

}


};

gdjs.New_32sceneCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.New_32sceneCode.GDbackObjects1.length = 0;
gdjs.New_32sceneCode.GDbackObjects2.length = 0;
gdjs.New_32sceneCode.GDvibrationlinkObjects1.length = 0;
gdjs.New_32sceneCode.GDvibrationlinkObjects2.length = 0;
gdjs.New_32sceneCode.GDupdownlinkObjects1.length = 0;
gdjs.New_32sceneCode.GDupdownlinkObjects2.length = 0;
gdjs.New_32sceneCode.GDtextlinkObjects1.length = 0;
gdjs.New_32sceneCode.GDtextlinkObjects2.length = 0;
gdjs.New_32sceneCode.GDvollinkObjects1.length = 0;
gdjs.New_32sceneCode.GDvollinkObjects2.length = 0;
gdjs.New_32sceneCode.GDmovelinkObjects1.length = 0;
gdjs.New_32sceneCode.GDmovelinkObjects2.length = 0;
gdjs.New_32sceneCode.GDlonglinkObjects1.length = 0;
gdjs.New_32sceneCode.GDlonglinkObjects2.length = 0;
gdjs.New_32sceneCode.GDtimelinkObjects1.length = 0;
gdjs.New_32sceneCode.GDtimelinkObjects2.length = 0;
gdjs.New_32sceneCode.GDbacklinkObjects1.length = 0;
gdjs.New_32sceneCode.GDbacklinkObjects2.length = 0;
gdjs.New_32sceneCode.GDwifilinkObjects1.length = 0;
gdjs.New_32sceneCode.GDwifilinkObjects2.length = 0;
gdjs.New_32sceneCode.GDsunlinkObjects1.length = 0;
gdjs.New_32sceneCode.GDsunlinkObjects2.length = 0;
gdjs.New_32sceneCode.GDinfolinkObjects1.length = 0;
gdjs.New_32sceneCode.GDinfolinkObjects2.length = 0;
gdjs.New_32sceneCode.GDinfoObjects1.length = 0;
gdjs.New_32sceneCode.GDinfoObjects2.length = 0;
gdjs.New_32sceneCode.GDmiclinkObjects1.length = 0;
gdjs.New_32sceneCode.GDmiclinkObjects2.length = 0;

gdjs.New_32sceneCode.eventsList0(runtimeScene);
return;

}

gdjs['New_32sceneCode'] = gdjs.New_32sceneCode;
