gdjs.evtsExt__Internet_Connectivity__isOnline = {};

gdjs.evtsExt__Internet_Connectivity__isOnline.conditionTrue_0 = {val:false};
gdjs.evtsExt__Internet_Connectivity__isOnline.condition0IsTrue_0 = {val:false};


gdjs.evtsExt__Internet_Connectivity__isOnline.userFunc0x8150d8 = function(runtimeScene, eventsFunctionContext) {
"use strict";
var status = runtimeScene.getVariables().get("connectionState");

window.addEventListener("online", connectionStatus);

function connectionStatus() {
    
    if (navigator.onLine) {
        return
    }
}

eventsFunctionContext.returnValue = navigator.onLine;

//made by TheGemDev (2020)


};
gdjs.evtsExt__Internet_Connectivity__isOnline.eventsList0 = function(runtimeScene, eventsFunctionContext) {

{


{
}

}


{


gdjs.evtsExt__Internet_Connectivity__isOnline.userFunc0x8150d8(runtimeScene, typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined);

}


};

gdjs.evtsExt__Internet_Connectivity__isOnline.func = function(runtimeScene, parentEventsFunctionContext) {
var eventsFunctionContext = {
  _objectsMap: {
},
  _objectArraysMap: {
},
  _behaviorNamesMap: {
},
  getObjects: function(objectName) {
    return eventsFunctionContext._objectArraysMap[objectName] || [];
  },
  getObjectsLists: function(objectName) {
    return eventsFunctionContext._objectsMap[objectName] || null;
  },
  getBehaviorName: function(behaviorName) {
    return eventsFunctionContext._behaviorNamesMap[behaviorName];
  },
  createObject: function(objectName) {
    var objectsList = eventsFunctionContext._objectsMap[objectName];
    if (objectsList) {
      return parentEventsFunctionContext ?
        parentEventsFunctionContext.createObject(objectsList.firstKey()) :
        runtimeScene.createObject(objectsList.firstKey());
    }
    return null;
  },
  getArgument: function(argName) {
    return "";
  },
  getOnceTriggers: function() { return runtimeScene.getOnceTriggers(); }
};


gdjs.evtsExt__Internet_Connectivity__isOnline.eventsList0(runtimeScene, eventsFunctionContext);
return !!eventsFunctionContext.returnValue;
}

