gdjs.New_32scene2Code = {};
gdjs.New_32scene2Code.GDbackObjects1= [];
gdjs.New_32scene2Code.GDbackObjects2= [];
gdjs.New_32scene2Code.GDerrorObjects1= [];
gdjs.New_32scene2Code.GDerrorObjects2= [];
gdjs.New_32scene2Code.GDRISKYObjects1= [];
gdjs.New_32scene2Code.GDRISKYObjects2= [];

gdjs.New_32scene2Code.conditionTrue_0 = {val:false};
gdjs.New_32scene2Code.condition0IsTrue_0 = {val:false};
gdjs.New_32scene2Code.condition1IsTrue_0 = {val:false};
gdjs.New_32scene2Code.condition2IsTrue_0 = {val:false};


gdjs.New_32scene2Code.mapOfGDgdjs_46New_9532scene2Code_46GDRISKYObjects1Objects = Hashtable.newFrom({"RISKY": gdjs.New_32scene2Code.GDRISKYObjects1});gdjs.New_32scene2Code.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("RISKY"), gdjs.New_32scene2Code.GDRISKYObjects1);

gdjs.New_32scene2Code.condition0IsTrue_0.val = false;
gdjs.New_32scene2Code.condition1IsTrue_0.val = false;
{
gdjs.New_32scene2Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.New_32scene2Code.mapOfGDgdjs_46New_9532scene2Code_46GDRISKYObjects1Objects, runtimeScene, true, false);
}if ( gdjs.New_32scene2Code.condition0IsTrue_0.val ) {
{
gdjs.New_32scene2Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.New_32scene2Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}{runtimeScene.getGame().getVariables().getFromIndex(13).setNumber(1);
}}

}


{


{
}

}


{


{
}

}


};

gdjs.New_32scene2Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.New_32scene2Code.GDbackObjects1.length = 0;
gdjs.New_32scene2Code.GDbackObjects2.length = 0;
gdjs.New_32scene2Code.GDerrorObjects1.length = 0;
gdjs.New_32scene2Code.GDerrorObjects2.length = 0;
gdjs.New_32scene2Code.GDRISKYObjects1.length = 0;
gdjs.New_32scene2Code.GDRISKYObjects2.length = 0;

gdjs.New_32scene2Code.eventsList0(runtimeScene);
return;

}

gdjs['New_32scene2Code'] = gdjs.New_32scene2Code;
