gdjs.textlevelCode = {};
gdjs.textlevelCode.GDanswerObjects1_1final = [];

gdjs.textlevelCode.GDbackObjects1= [];
gdjs.textlevelCode.GDbackObjects2= [];
gdjs.textlevelCode.GDtextlevelinputObjects1= [];
gdjs.textlevelCode.GDtextlevelinputObjects2= [];
gdjs.textlevelCode.GDanswerObjects1= [];
gdjs.textlevelCode.GDanswerObjects2= [];
gdjs.textlevelCode.GDquestionObjects1= [];
gdjs.textlevelCode.GDquestionObjects2= [];

gdjs.textlevelCode.conditionTrue_0 = {val:false};
gdjs.textlevelCode.condition0IsTrue_0 = {val:false};
gdjs.textlevelCode.condition1IsTrue_0 = {val:false};
gdjs.textlevelCode.condition2IsTrue_0 = {val:false};
gdjs.textlevelCode.conditionTrue_1 = {val:false};
gdjs.textlevelCode.condition0IsTrue_1 = {val:false};
gdjs.textlevelCode.condition1IsTrue_1 = {val:false};
gdjs.textlevelCode.condition2IsTrue_1 = {val:false};


gdjs.textlevelCode.mapOfGDgdjs_46textlevelCode_46GDbackObjects1Objects = Hashtable.newFrom({"back": gdjs.textlevelCode.GDbackObjects1});gdjs.textlevelCode.eventsList0 = function(runtimeScene) {

{


gdjs.textlevelCode.condition0IsTrue_0.val = false;
{
gdjs.textlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.textlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "0;0;0");
}}

}


{


gdjs.textlevelCode.condition0IsTrue_0.val = false;
{
gdjs.textlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.textlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "255;255;255");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.textlevelCode.GDbackObjects1);

gdjs.textlevelCode.condition0IsTrue_0.val = false;
{
gdjs.textlevelCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.textlevelCode.mapOfGDgdjs_46textlevelCode_46GDbackObjects1Objects, runtimeScene, true, false);
}if (gdjs.textlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


{


{
gdjs.copyArray(runtimeScene.getObjects("answer"), gdjs.textlevelCode.GDanswerObjects1);
gdjs.copyArray(runtimeScene.getObjects("textlevelinput"), gdjs.textlevelCode.GDtextlevelinputObjects1);
{for(var i = 0, len = gdjs.textlevelCode.GDtextlevelinputObjects1.length ;i < len;++i) {
    gdjs.textlevelCode.GDtextlevelinputObjects1[i].getBehavior("TextEntryVirtualKeyboard").openKeyboard((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{for(var i = 0, len = gdjs.textlevelCode.GDanswerObjects1.length ;i < len;++i) {
    gdjs.textlevelCode.GDanswerObjects1[i].setString((( gdjs.textlevelCode.GDtextlevelinputObjects1.length === 0 ) ? "" :gdjs.textlevelCode.GDtextlevelinputObjects1[0].getString()));
}
}}

}


{

gdjs.textlevelCode.GDanswerObjects1.length = 0;


gdjs.textlevelCode.condition0IsTrue_0.val = false;
gdjs.textlevelCode.condition1IsTrue_0.val = false;
{
{gdjs.textlevelCode.conditionTrue_1 = gdjs.textlevelCode.condition0IsTrue_0;
gdjs.textlevelCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8593804);
}
}if ( gdjs.textlevelCode.condition0IsTrue_0.val ) {
{
{gdjs.textlevelCode.conditionTrue_1 = gdjs.textlevelCode.condition1IsTrue_0;
gdjs.textlevelCode.GDanswerObjects1_1final.length = 0;gdjs.textlevelCode.condition0IsTrue_1.val = false;
{
gdjs.copyArray(runtimeScene.getObjects("answer"), gdjs.textlevelCode.GDanswerObjects2);
for(var i = 0, k = 0, l = gdjs.textlevelCode.GDanswerObjects2.length;i<l;++i) {
    if ( gdjs.textlevelCode.GDanswerObjects2[i].getString() == "no" ) {
        gdjs.textlevelCode.condition0IsTrue_1.val = true;
        gdjs.textlevelCode.GDanswerObjects2[k] = gdjs.textlevelCode.GDanswerObjects2[i];
        ++k;
    }
}
gdjs.textlevelCode.GDanswerObjects2.length = k;if( gdjs.textlevelCode.condition0IsTrue_1.val ) {
    gdjs.textlevelCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.textlevelCode.GDanswerObjects2.length;j<jLen;++j) {
        if ( gdjs.textlevelCode.GDanswerObjects1_1final.indexOf(gdjs.textlevelCode.GDanswerObjects2[j]) === -1 )
            gdjs.textlevelCode.GDanswerObjects1_1final.push(gdjs.textlevelCode.GDanswerObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs.textlevelCode.GDanswerObjects1_1final, gdjs.textlevelCode.GDanswerObjects1);
}
}
}}
if (gdjs.textlevelCode.condition1IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("textlevelinput"), gdjs.textlevelCode.GDtextlevelinputObjects1);
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}{for(var i = 0, len = gdjs.textlevelCode.GDtextlevelinputObjects1.length ;i < len;++i) {
    gdjs.textlevelCode.GDtextlevelinputObjects1[i].getBehavior("TextEntryVirtualKeyboard").closeKeyboard((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


};

gdjs.textlevelCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.textlevelCode.GDbackObjects1.length = 0;
gdjs.textlevelCode.GDbackObjects2.length = 0;
gdjs.textlevelCode.GDtextlevelinputObjects1.length = 0;
gdjs.textlevelCode.GDtextlevelinputObjects2.length = 0;
gdjs.textlevelCode.GDanswerObjects1.length = 0;
gdjs.textlevelCode.GDanswerObjects2.length = 0;
gdjs.textlevelCode.GDquestionObjects1.length = 0;
gdjs.textlevelCode.GDquestionObjects2.length = 0;

gdjs.textlevelCode.eventsList0(runtimeScene);
return;

}

gdjs['textlevelCode'] = gdjs.textlevelCode;
