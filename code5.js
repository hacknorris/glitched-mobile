gdjs.soundlevelCode = {};
gdjs.soundlevelCode.GDbackObjects1= [];
gdjs.soundlevelCode.GDbackObjects2= [];
gdjs.soundlevelCode.GDmoresoundObjects1= [];
gdjs.soundlevelCode.GDmoresoundObjects2= [];

gdjs.soundlevelCode.conditionTrue_0 = {val:false};
gdjs.soundlevelCode.condition0IsTrue_0 = {val:false};
gdjs.soundlevelCode.condition1IsTrue_0 = {val:false};


gdjs.soundlevelCode.mapOfGDgdjs_46soundlevelCode_46GDbackObjects1Objects = Hashtable.newFrom({"back": gdjs.soundlevelCode.GDbackObjects1});gdjs.soundlevelCode.eventsList0 = function(runtimeScene) {

{


gdjs.soundlevelCode.condition0IsTrue_0.val = false;
{
gdjs.soundlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.soundlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "0;0;0");
}}

}


{


gdjs.soundlevelCode.condition0IsTrue_0.val = false;
{
gdjs.soundlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.soundlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "255;255;255");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.soundlevelCode.GDbackObjects1);

gdjs.soundlevelCode.condition0IsTrue_0.val = false;
{
gdjs.soundlevelCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.soundlevelCode.mapOfGDgdjs_46soundlevelCode_46GDbackObjects1Objects, runtimeScene, true, false);
}if (gdjs.soundlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


{


{
{runtimeScene.getGame().getVariables().getFromIndex(4).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


};

gdjs.soundlevelCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.soundlevelCode.GDbackObjects1.length = 0;
gdjs.soundlevelCode.GDbackObjects2.length = 0;
gdjs.soundlevelCode.GDmoresoundObjects1.length = 0;
gdjs.soundlevelCode.GDmoresoundObjects2.length = 0;

gdjs.soundlevelCode.eventsList0(runtimeScene);
return;

}

gdjs['soundlevelCode'] = gdjs.soundlevelCode;
