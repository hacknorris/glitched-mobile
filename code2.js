gdjs.shakelevelCode = {};
gdjs.shakelevelCode.GDbackObjects1= [];
gdjs.shakelevelCode.GDbackObjects2= [];

gdjs.shakelevelCode.conditionTrue_0 = {val:false};
gdjs.shakelevelCode.condition0IsTrue_0 = {val:false};
gdjs.shakelevelCode.condition1IsTrue_0 = {val:false};


gdjs.shakelevelCode.mapOfGDgdjs_46shakelevelCode_46GDbackObjects1Objects = Hashtable.newFrom({"back": gdjs.shakelevelCode.GDbackObjects1});gdjs.shakelevelCode.eventsList0 = function(runtimeScene) {

{


gdjs.shakelevelCode.condition0IsTrue_0.val = false;
{
gdjs.shakelevelCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.shakelevelCode.condition0IsTrue_0.val) {
{gdjs.deviceSensors.motion.activateMotionSensor();
}}

}


{


gdjs.shakelevelCode.condition0IsTrue_0.val = false;
{
gdjs.shakelevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.shakelevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "0;0;0");
}}

}


{


gdjs.shakelevelCode.condition0IsTrue_0.val = false;
{
gdjs.shakelevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.shakelevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "255;255;255");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.shakelevelCode.GDbackObjects1);

gdjs.shakelevelCode.condition0IsTrue_0.val = false;
{
gdjs.shakelevelCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.shakelevelCode.mapOfGDgdjs_46shakelevelCode_46GDbackObjects1Objects, runtimeScene, true, false);
}if (gdjs.shakelevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", true);
}}

}


{


gdjs.shakelevelCode.condition0IsTrue_0.val = false;
{
gdjs.shakelevelCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.shakelevelCode.condition0IsTrue_0.val) {
{gdjs.deviceVibration.startVibration(10000);
}}

}


{


gdjs.shakelevelCode.condition0IsTrue_0.val = false;
{
gdjs.shakelevelCode.condition0IsTrue_0.val = gdjs.deviceSensors.motion.getRotationAlpha(">=", 0.2);
}if (gdjs.shakelevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", true);
}}

}


};

gdjs.shakelevelCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.shakelevelCode.GDbackObjects1.length = 0;
gdjs.shakelevelCode.GDbackObjects2.length = 0;

gdjs.shakelevelCode.eventsList0(runtimeScene);
return;

}

gdjs['shakelevelCode'] = gdjs.shakelevelCode;
