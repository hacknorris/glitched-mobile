gdjs.netlevelCode = {};
gdjs.netlevelCode.GDbackObjects1= [];
gdjs.netlevelCode.GDbackObjects2= [];
gdjs.netlevelCode.GDofflineObjects1= [];
gdjs.netlevelCode.GDofflineObjects2= [];

gdjs.netlevelCode.conditionTrue_0 = {val:false};
gdjs.netlevelCode.condition0IsTrue_0 = {val:false};
gdjs.netlevelCode.condition1IsTrue_0 = {val:false};


gdjs.netlevelCode.mapOfGDgdjs_46netlevelCode_46GDbackObjects1Objects = Hashtable.newFrom({"back": gdjs.netlevelCode.GDbackObjects1});gdjs.netlevelCode.eventsList0 = function(runtimeScene) {

{


gdjs.netlevelCode.condition0IsTrue_0.val = false;
{
gdjs.netlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.netlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "0;0;0");
}}

}


{


gdjs.netlevelCode.condition0IsTrue_0.val = false;
{
gdjs.netlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.netlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "255;255;255");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.netlevelCode.GDbackObjects1);

gdjs.netlevelCode.condition0IsTrue_0.val = false;
{
gdjs.netlevelCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.netlevelCode.mapOfGDgdjs_46netlevelCode_46GDbackObjects1Objects, runtimeScene, true, false);
}if (gdjs.netlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


{


gdjs.netlevelCode.condition0IsTrue_0.val = false;
{
gdjs.netlevelCode.condition0IsTrue_0.val = !(gdjs.evtsExt__Internet_Connectivity__isOnline.func(runtimeScene, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)));
}if (gdjs.netlevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(9).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


};

gdjs.netlevelCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.netlevelCode.GDbackObjects1.length = 0;
gdjs.netlevelCode.GDbackObjects2.length = 0;
gdjs.netlevelCode.GDofflineObjects1.length = 0;
gdjs.netlevelCode.GDofflineObjects2.length = 0;

gdjs.netlevelCode.eventsList0(runtimeScene);
return;

}

gdjs['netlevelCode'] = gdjs.netlevelCode;
