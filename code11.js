gdjs.starlevelCode = {};
gdjs.starlevelCode.GDbackObjects1= [];
gdjs.starlevelCode.GDbackObjects2= [];
gdjs.starlevelCode.GDskylookObjects1= [];
gdjs.starlevelCode.GDskylookObjects2= [];

gdjs.starlevelCode.conditionTrue_0 = {val:false};
gdjs.starlevelCode.condition0IsTrue_0 = {val:false};
gdjs.starlevelCode.condition1IsTrue_0 = {val:false};


gdjs.starlevelCode.mapOfGDgdjs_46starlevelCode_46GDbackObjects1Objects = Hashtable.newFrom({"back": gdjs.starlevelCode.GDbackObjects1});gdjs.starlevelCode.eventsList0 = function(runtimeScene) {

{


gdjs.starlevelCode.condition0IsTrue_0.val = false;
{
gdjs.starlevelCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.starlevelCode.condition0IsTrue_0.val) {
{gdjs.deviceSensors.orientation.activateOrientationSensor();
}}

}


{


gdjs.starlevelCode.condition0IsTrue_0.val = false;
{
gdjs.starlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.starlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "0;0;0");
}}

}


{


gdjs.starlevelCode.condition0IsTrue_0.val = false;
{
gdjs.starlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.starlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "255;255;255");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.starlevelCode.GDbackObjects1);

gdjs.starlevelCode.condition0IsTrue_0.val = false;
{
gdjs.starlevelCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.starlevelCode.mapOfGDgdjs_46starlevelCode_46GDbackObjects1Objects, runtimeScene, true, false);
}if (gdjs.starlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


{


gdjs.starlevelCode.condition0IsTrue_0.val = false;
{
gdjs.starlevelCode.condition0IsTrue_0.val = gdjs.deviceSensors.orientation.getOrientationBeta("==", 180);
}if (gdjs.starlevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(10).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


{


gdjs.starlevelCode.condition0IsTrue_0.val = false;
{
gdjs.starlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.starlevelCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("skylook"), gdjs.starlevelCode.GDskylookObjects1);
{for(var i = 0, len = gdjs.starlevelCode.GDskylookObjects1.length ;i < len;++i) {
    gdjs.starlevelCode.GDskylookObjects1[i].setAnimation(0);
}
}}

}


{


gdjs.starlevelCode.condition0IsTrue_0.val = false;
{
gdjs.starlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.starlevelCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("skylook"), gdjs.starlevelCode.GDskylookObjects1);
{for(var i = 0, len = gdjs.starlevelCode.GDskylookObjects1.length ;i < len;++i) {
    gdjs.starlevelCode.GDskylookObjects1[i].setAnimation(1);
}
}}

}


};

gdjs.starlevelCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.starlevelCode.GDbackObjects1.length = 0;
gdjs.starlevelCode.GDbackObjects2.length = 0;
gdjs.starlevelCode.GDskylookObjects1.length = 0;
gdjs.starlevelCode.GDskylookObjects2.length = 0;

gdjs.starlevelCode.eventsList0(runtimeScene);
return;

}

gdjs['starlevelCode'] = gdjs.starlevelCode;
