gdjs.longlevelCode = {};
gdjs.longlevelCode.GDbackObjects1= [];
gdjs.longlevelCode.GDbackObjects2= [];
gdjs.longlevelCode.GDbuttonObjects1= [];
gdjs.longlevelCode.GDbuttonObjects2= [];
gdjs.longlevelCode.GDrealbuttonObjects1= [];
gdjs.longlevelCode.GDrealbuttonObjects2= [];

gdjs.longlevelCode.conditionTrue_0 = {val:false};
gdjs.longlevelCode.condition0IsTrue_0 = {val:false};
gdjs.longlevelCode.condition1IsTrue_0 = {val:false};
gdjs.longlevelCode.condition2IsTrue_0 = {val:false};
gdjs.longlevelCode.conditionTrue_1 = {val:false};
gdjs.longlevelCode.condition0IsTrue_1 = {val:false};
gdjs.longlevelCode.condition1IsTrue_1 = {val:false};
gdjs.longlevelCode.condition2IsTrue_1 = {val:false};


gdjs.longlevelCode.mapOfGDgdjs_46longlevelCode_46GDbackObjects1Objects = Hashtable.newFrom({"back": gdjs.longlevelCode.GDbackObjects1});gdjs.longlevelCode.mapOfGDgdjs_46longlevelCode_46GDrealbuttonObjects1Objects = Hashtable.newFrom({"realbutton": gdjs.longlevelCode.GDrealbuttonObjects1});gdjs.longlevelCode.mapOfGDgdjs_46longlevelCode_46GDrealbuttonObjects1Objects = Hashtable.newFrom({"realbutton": gdjs.longlevelCode.GDrealbuttonObjects1});gdjs.longlevelCode.eventsList0 = function(runtimeScene) {

{


gdjs.longlevelCode.condition0IsTrue_0.val = false;
{
gdjs.longlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.longlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "0;0;0");
}}

}


{


gdjs.longlevelCode.condition0IsTrue_0.val = false;
{
gdjs.longlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.longlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "255;255;255");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.longlevelCode.GDbackObjects1);

gdjs.longlevelCode.condition0IsTrue_0.val = false;
{
gdjs.longlevelCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.longlevelCode.mapOfGDgdjs_46longlevelCode_46GDbackObjects1Objects, runtimeScene, true, false);
}if (gdjs.longlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("realbutton"), gdjs.longlevelCode.GDrealbuttonObjects1);

gdjs.longlevelCode.condition0IsTrue_0.val = false;
gdjs.longlevelCode.condition1IsTrue_0.val = false;
{
{gdjs.longlevelCode.conditionTrue_1 = gdjs.longlevelCode.condition0IsTrue_0;
gdjs.longlevelCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8609052);
}
}if ( gdjs.longlevelCode.condition0IsTrue_0.val ) {
{
gdjs.longlevelCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.longlevelCode.mapOfGDgdjs_46longlevelCode_46GDrealbuttonObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.longlevelCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "long");
}}

}


{


gdjs.longlevelCode.condition0IsTrue_0.val = false;
{
{gdjs.longlevelCode.conditionTrue_1 = gdjs.longlevelCode.condition0IsTrue_0;
gdjs.longlevelCode.condition0IsTrue_1.val = false;
gdjs.longlevelCode.condition1IsTrue_1.val = false;
{
gdjs.longlevelCode.condition0IsTrue_1.val = gdjs.evtTools.input.popEndedTouch(runtimeScene);
}if ( gdjs.longlevelCode.condition0IsTrue_1.val ) {
{
gdjs.longlevelCode.condition1IsTrue_1.val = !(gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 5, "long"));
}}
gdjs.longlevelCode.conditionTrue_1.val = true && gdjs.longlevelCode.condition0IsTrue_1.val && gdjs.longlevelCode.condition1IsTrue_1.val;
}
}if (gdjs.longlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "long");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("realbutton"), gdjs.longlevelCode.GDrealbuttonObjects1);

gdjs.longlevelCode.condition0IsTrue_0.val = false;
{
{gdjs.longlevelCode.conditionTrue_1 = gdjs.longlevelCode.condition0IsTrue_0;
gdjs.longlevelCode.condition0IsTrue_1.val = false;
gdjs.longlevelCode.condition1IsTrue_1.val = false;
{
gdjs.longlevelCode.condition0IsTrue_1.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 5, "long");
}if ( gdjs.longlevelCode.condition0IsTrue_1.val ) {
{
gdjs.longlevelCode.condition1IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.longlevelCode.mapOfGDgdjs_46longlevelCode_46GDrealbuttonObjects1Objects, runtimeScene, true, false);
}}
gdjs.longlevelCode.conditionTrue_1.val = true && gdjs.longlevelCode.condition0IsTrue_1.val && gdjs.longlevelCode.condition1IsTrue_1.val;
}
}if (gdjs.longlevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(6).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", true);
}}

}


};

gdjs.longlevelCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.longlevelCode.GDbackObjects1.length = 0;
gdjs.longlevelCode.GDbackObjects2.length = 0;
gdjs.longlevelCode.GDbuttonObjects1.length = 0;
gdjs.longlevelCode.GDbuttonObjects2.length = 0;
gdjs.longlevelCode.GDrealbuttonObjects1.length = 0;
gdjs.longlevelCode.GDrealbuttonObjects2.length = 0;

gdjs.longlevelCode.eventsList0(runtimeScene);
return;

}

gdjs['longlevelCode'] = gdjs.longlevelCode;
