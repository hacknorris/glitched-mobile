gdjs.infoCode = {};
gdjs.infoCode.GDbackObjects1= [];
gdjs.infoCode.GDbackObjects2= [];
gdjs.infoCode.GDlevel_95info_95winnerObjects1= [];
gdjs.infoCode.GDlevel_95info_95winnerObjects2= [];
gdjs.infoCode.GDdsclinkObjects1= [];
gdjs.infoCode.GDdsclinkObjects2= [];
gdjs.infoCode.GDfblinkObjects1= [];
gdjs.infoCode.GDfblinkObjects2= [];
gdjs.infoCode.GDinfoObjects1= [];
gdjs.infoCode.GDinfoObjects2= [];
gdjs.infoCode.GDbuildObjects1= [];
gdjs.infoCode.GDbuildObjects2= [];

gdjs.infoCode.conditionTrue_0 = {val:false};
gdjs.infoCode.condition0IsTrue_0 = {val:false};
gdjs.infoCode.condition1IsTrue_0 = {val:false};


gdjs.infoCode.mapOfGDgdjs_46infoCode_46GDbackObjects1Objects = Hashtable.newFrom({"back": gdjs.infoCode.GDbackObjects1});gdjs.infoCode.mapOfGDgdjs_46infoCode_46GDlevel_9595info_9595winnerObjects1Objects = Hashtable.newFrom({"level_info_winner": gdjs.infoCode.GDlevel_95info_95winnerObjects1});gdjs.infoCode.mapOfGDgdjs_46infoCode_46GDdsclinkObjects1Objects = Hashtable.newFrom({"dsclink": gdjs.infoCode.GDdsclinkObjects1});gdjs.infoCode.mapOfGDgdjs_46infoCode_46GDfblinkObjects1Objects = Hashtable.newFrom({"fblink": gdjs.infoCode.GDfblinkObjects1});gdjs.infoCode.eventsList0 = function(runtimeScene) {

{


gdjs.infoCode.condition0IsTrue_0.val = false;
{
gdjs.infoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.infoCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "0;0;0");
}}

}


{


gdjs.infoCode.condition0IsTrue_0.val = false;
{
gdjs.infoCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.infoCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "255;255;255");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.infoCode.GDbackObjects1);

gdjs.infoCode.condition0IsTrue_0.val = false;
{
gdjs.infoCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infoCode.mapOfGDgdjs_46infoCode_46GDbackObjects1Objects, runtimeScene, true, false);
}if (gdjs.infoCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("level_info_winner"), gdjs.infoCode.GDlevel_95info_95winnerObjects1);

gdjs.infoCode.condition0IsTrue_0.val = false;
{
gdjs.infoCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infoCode.mapOfGDgdjs_46infoCode_46GDlevel_9595info_9595winnerObjects1Objects, runtimeScene, true, false);
}if (gdjs.infoCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(11).setNumber(1);
}}

}

{

gdjs.copyArray(runtimeScene.getObjects("fblink"), gdjs.infoCode.GDfblinkObjects1);

gdjs.infoCode.condition0IsTrue_0.val = false;
{
gdjs.infoCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.infoCode.mapOfGDgdjs_46infoCode_46GDfblinkObjects1Objects, runtimeScene, true, false);
}if (gdjs.infoCode.condition0IsTrue_0.val) {
{gdjs.evtTools.window.openURL("https://www.facebook.com/hacks.norris/", runtimeScene);
}}

}


};

gdjs.infoCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.infoCode.GDbackObjects1.length = 0;
gdjs.infoCode.GDbackObjects2.length = 0;
gdjs.infoCode.GDlevel_95info_95winnerObjects1.length = 0;
gdjs.infoCode.GDlevel_95info_95winnerObjects2.length = 0;
gdjs.infoCode.GDdsclinkObjects1.length = 0;
gdjs.infoCode.GDdsclinkObjects2.length = 0;
gdjs.infoCode.GDfblinkObjects1.length = 0;
gdjs.infoCode.GDfblinkObjects2.length = 0;
gdjs.infoCode.GDinfoObjects1.length = 0;
gdjs.infoCode.GDinfoObjects2.length = 0;
gdjs.infoCode.GDbuildObjects1.length = 0;
gdjs.infoCode.GDbuildObjects2.length = 0;

gdjs.infoCode.eventsList0(runtimeScene);
return;

}

gdjs['infoCode'] = gdjs.infoCode;
