gdjs.landscapelevelCode = {};
gdjs.landscapelevelCode.GDbackObjects1= [];
gdjs.landscapelevelCode.GDbackObjects2= [];
gdjs.landscapelevelCode.GDlandscapeObjects1= [];
gdjs.landscapelevelCode.GDlandscapeObjects2= [];

gdjs.landscapelevelCode.conditionTrue_0 = {val:false};
gdjs.landscapelevelCode.condition0IsTrue_0 = {val:false};
gdjs.landscapelevelCode.condition1IsTrue_0 = {val:false};
gdjs.landscapelevelCode.condition2IsTrue_0 = {val:false};
gdjs.landscapelevelCode.condition3IsTrue_0 = {val:false};
gdjs.landscapelevelCode.conditionTrue_1 = {val:false};
gdjs.landscapelevelCode.condition0IsTrue_1 = {val:false};
gdjs.landscapelevelCode.condition1IsTrue_1 = {val:false};
gdjs.landscapelevelCode.condition2IsTrue_1 = {val:false};
gdjs.landscapelevelCode.condition3IsTrue_1 = {val:false};
gdjs.landscapelevelCode.conditionTrue_2 = {val:false};
gdjs.landscapelevelCode.condition0IsTrue_2 = {val:false};
gdjs.landscapelevelCode.condition1IsTrue_2 = {val:false};
gdjs.landscapelevelCode.condition2IsTrue_2 = {val:false};
gdjs.landscapelevelCode.condition3IsTrue_2 = {val:false};


gdjs.landscapelevelCode.mapOfGDgdjs_46landscapelevelCode_46GDbackObjects1Objects = Hashtable.newFrom({"back": gdjs.landscapelevelCode.GDbackObjects1});gdjs.landscapelevelCode.eventsList0 = function(runtimeScene) {

{


gdjs.landscapelevelCode.condition0IsTrue_0.val = false;
{
gdjs.landscapelevelCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.landscapelevelCode.condition0IsTrue_0.val) {
{gdjs.deviceSensors.orientation.activateOrientationSensor();
}}

}


{


gdjs.landscapelevelCode.condition0IsTrue_0.val = false;
{
gdjs.landscapelevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.landscapelevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "0;0;0");
}}

}


{


gdjs.landscapelevelCode.condition0IsTrue_0.val = false;
{
gdjs.landscapelevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.landscapelevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "255;255;255");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.landscapelevelCode.GDbackObjects1);

gdjs.landscapelevelCode.condition0IsTrue_0.val = false;
{
gdjs.landscapelevelCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.landscapelevelCode.mapOfGDgdjs_46landscapelevelCode_46GDbackObjects1Objects, runtimeScene, true, false);
}if (gdjs.landscapelevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


{


gdjs.landscapelevelCode.condition0IsTrue_0.val = false;
{
{gdjs.landscapelevelCode.conditionTrue_1 = gdjs.landscapelevelCode.condition0IsTrue_0;
gdjs.landscapelevelCode.condition0IsTrue_1.val = false;
gdjs.landscapelevelCode.condition1IsTrue_1.val = false;
gdjs.landscapelevelCode.condition2IsTrue_1.val = false;
{
{gdjs.landscapelevelCode.conditionTrue_2 = gdjs.landscapelevelCode.condition0IsTrue_1;
gdjs.landscapelevelCode.conditionTrue_2.val = (gdjs.evtTools.window.getWindowInnerHeight() < gdjs.evtTools.window.getWindowInnerWidth());
}
if( gdjs.landscapelevelCode.condition0IsTrue_1.val ) {
    gdjs.landscapelevelCode.conditionTrue_1.val = true;
}
}
{
gdjs.landscapelevelCode.condition1IsTrue_1.val = gdjs.deviceSensors.orientation.getOrientationGamma("==", 90);
if( gdjs.landscapelevelCode.condition1IsTrue_1.val ) {
    gdjs.landscapelevelCode.conditionTrue_1.val = true;
}
}
{
gdjs.landscapelevelCode.condition2IsTrue_1.val = gdjs.deviceSensors.orientation.getOrientationGamma("==", -(90));
if( gdjs.landscapelevelCode.condition2IsTrue_1.val ) {
    gdjs.landscapelevelCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.landscapelevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


};

gdjs.landscapelevelCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.landscapelevelCode.GDbackObjects1.length = 0;
gdjs.landscapelevelCode.GDbackObjects2.length = 0;
gdjs.landscapelevelCode.GDlandscapeObjects1.length = 0;
gdjs.landscapelevelCode.GDlandscapeObjects2.length = 0;

gdjs.landscapelevelCode.eventsList0(runtimeScene);
return;

}

gdjs['landscapelevelCode'] = gdjs.landscapelevelCode;
