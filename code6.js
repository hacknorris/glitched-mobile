gdjs.movelevelCode = {};
gdjs.movelevelCode.GDbackObjects1= [];
gdjs.movelevelCode.GDbackObjects2= [];
gdjs.movelevelCode.GDforwardObjects1= [];
gdjs.movelevelCode.GDforwardObjects2= [];

gdjs.movelevelCode.conditionTrue_0 = {val:false};
gdjs.movelevelCode.condition0IsTrue_0 = {val:false};
gdjs.movelevelCode.condition1IsTrue_0 = {val:false};


gdjs.movelevelCode.mapOfGDgdjs_46movelevelCode_46GDbackObjects1Objects = Hashtable.newFrom({"back": gdjs.movelevelCode.GDbackObjects1});gdjs.movelevelCode.eventsList0 = function(runtimeScene) {

{


gdjs.movelevelCode.condition0IsTrue_0.val = false;
{
gdjs.movelevelCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.movelevelCode.condition0IsTrue_0.val) {
{gdjs.deviceSensors.motion.activateMotionSensor();
}}

}


{


gdjs.movelevelCode.condition0IsTrue_0.val = false;
{
gdjs.movelevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.movelevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "0;0;0");
}}

}


{


gdjs.movelevelCode.condition0IsTrue_0.val = false;
{
gdjs.movelevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.movelevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "255;255;255");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.movelevelCode.GDbackObjects1);

gdjs.movelevelCode.condition0IsTrue_0.val = false;
{
gdjs.movelevelCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.movelevelCode.mapOfGDgdjs_46movelevelCode_46GDbackObjects1Objects, runtimeScene, true, false);
}if (gdjs.movelevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


{


gdjs.movelevelCode.condition0IsTrue_0.val = false;
{
gdjs.movelevelCode.condition0IsTrue_0.val = gdjs.deviceSensors.motion.getAccelerationY(">=", 0.2);
}if (gdjs.movelevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


};

gdjs.movelevelCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.movelevelCode.GDbackObjects1.length = 0;
gdjs.movelevelCode.GDbackObjects2.length = 0;
gdjs.movelevelCode.GDforwardObjects1.length = 0;
gdjs.movelevelCode.GDforwardObjects2.length = 0;

gdjs.movelevelCode.eventsList0(runtimeScene);
return;

}

gdjs['movelevelCode'] = gdjs.movelevelCode;
