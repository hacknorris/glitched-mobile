gdjs.microlevelCode = {};
gdjs.microlevelCode.GDbackObjects1= [];
gdjs.microlevelCode.GDbackObjects2= [];
gdjs.microlevelCode.GDmicroObjects1= [];
gdjs.microlevelCode.GDmicroObjects2= [];

gdjs.microlevelCode.conditionTrue_0 = {val:false};
gdjs.microlevelCode.condition0IsTrue_0 = {val:false};
gdjs.microlevelCode.condition1IsTrue_0 = {val:false};
gdjs.microlevelCode.conditionTrue_1 = {val:false};
gdjs.microlevelCode.condition0IsTrue_1 = {val:false};
gdjs.microlevelCode.condition1IsTrue_1 = {val:false};


gdjs.microlevelCode.mapOfGDgdjs_46microlevelCode_46GDbackObjects1Objects = Hashtable.newFrom({"back": gdjs.microlevelCode.GDbackObjects1});gdjs.microlevelCode.eventsList0 = function(runtimeScene) {

{


gdjs.microlevelCode.condition0IsTrue_0.val = false;
{
gdjs.microlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.microlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "0;0;0");
}}

}


{


gdjs.microlevelCode.condition0IsTrue_0.val = false;
{
gdjs.microlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.microlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "255;255;255");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.microlevelCode.GDbackObjects1);

gdjs.microlevelCode.condition0IsTrue_0.val = false;
{
gdjs.microlevelCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.microlevelCode.mapOfGDgdjs_46microlevelCode_46GDbackObjects1Objects, runtimeScene, true, false);
}if (gdjs.microlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


{


gdjs.microlevelCode.condition0IsTrue_0.val = false;
{
{gdjs.microlevelCode.conditionTrue_1 = gdjs.microlevelCode.condition0IsTrue_0;
gdjs.microlevelCode.conditionTrue_1.val = (gdjs.evtsExt__Micro__Loundness.func(runtimeScene, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) >= 0.5);
}
}if (gdjs.microlevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(12).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


};

gdjs.microlevelCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.microlevelCode.GDbackObjects1.length = 0;
gdjs.microlevelCode.GDbackObjects2.length = 0;
gdjs.microlevelCode.GDmicroObjects1.length = 0;
gdjs.microlevelCode.GDmicroObjects2.length = 0;

gdjs.microlevelCode.eventsList0(runtimeScene);
return;

}

gdjs['microlevelCode'] = gdjs.microlevelCode;
