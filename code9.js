gdjs.backlevelCode = {};
gdjs.backlevelCode.GDbackObjects1= [];
gdjs.backlevelCode.GDbackObjects2= [];

gdjs.backlevelCode.conditionTrue_0 = {val:false};
gdjs.backlevelCode.condition0IsTrue_0 = {val:false};
gdjs.backlevelCode.condition1IsTrue_0 = {val:false};


gdjs.backlevelCode.mapOfGDgdjs_46backlevelCode_46GDbackObjects1Objects = Hashtable.newFrom({"back": gdjs.backlevelCode.GDbackObjects1});gdjs.backlevelCode.eventsList0 = function(runtimeScene) {

{


gdjs.backlevelCode.condition0IsTrue_0.val = false;
{
gdjs.backlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.backlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "0;0;0");
}}

}


{


gdjs.backlevelCode.condition0IsTrue_0.val = false;
{
gdjs.backlevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.backlevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "255;255;255");
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.backlevelCode.GDbackObjects1);

gdjs.backlevelCode.condition0IsTrue_0.val = false;
{
gdjs.backlevelCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.backlevelCode.mapOfGDgdjs_46backlevelCode_46GDbackObjects1Objects, runtimeScene, true, false);
}if (gdjs.backlevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(8).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


};

gdjs.backlevelCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.backlevelCode.GDbackObjects1.length = 0;
gdjs.backlevelCode.GDbackObjects2.length = 0;

gdjs.backlevelCode.eventsList0(runtimeScene);
return;

}

gdjs['backlevelCode'] = gdjs.backlevelCode;
