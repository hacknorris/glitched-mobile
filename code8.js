gdjs.timelevelCode = {};
gdjs.timelevelCode.GDbackObjects1_1final = [];

gdjs.timelevelCode.GDbackObjects1= [];
gdjs.timelevelCode.GDbackObjects2= [];
gdjs.timelevelCode.GDtimetextObjects1= [];
gdjs.timelevelCode.GDtimetextObjects2= [];

gdjs.timelevelCode.conditionTrue_0 = {val:false};
gdjs.timelevelCode.condition0IsTrue_0 = {val:false};
gdjs.timelevelCode.condition1IsTrue_0 = {val:false};
gdjs.timelevelCode.condition2IsTrue_0 = {val:false};
gdjs.timelevelCode.conditionTrue_1 = {val:false};
gdjs.timelevelCode.condition0IsTrue_1 = {val:false};
gdjs.timelevelCode.condition1IsTrue_1 = {val:false};
gdjs.timelevelCode.condition2IsTrue_1 = {val:false};


gdjs.timelevelCode.mapOfGDgdjs_46timelevelCode_46GDbackObjects2Objects = Hashtable.newFrom({"back": gdjs.timelevelCode.GDbackObjects2});gdjs.timelevelCode.eventsList0 = function(runtimeScene) {

{


gdjs.timelevelCode.condition0IsTrue_0.val = false;
{
gdjs.timelevelCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.timelevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "timerlevel");
}}

}


{


gdjs.timelevelCode.condition0IsTrue_0.val = false;
{
gdjs.timelevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.timelevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "0;0;0");
}}

}


{


gdjs.timelevelCode.condition0IsTrue_0.val = false;
{
gdjs.timelevelCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.timelevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.setBackgroundColor(runtimeScene, "255;255;255");
}}

}


{

gdjs.timelevelCode.GDbackObjects1.length = 0;


gdjs.timelevelCode.condition0IsTrue_0.val = false;
{
{gdjs.timelevelCode.conditionTrue_1 = gdjs.timelevelCode.condition0IsTrue_0;
gdjs.timelevelCode.GDbackObjects1_1final.length = 0;gdjs.timelevelCode.condition0IsTrue_1.val = false;
gdjs.timelevelCode.condition1IsTrue_1.val = false;
{
gdjs.copyArray(runtimeScene.getObjects("back"), gdjs.timelevelCode.GDbackObjects2);
gdjs.timelevelCode.condition0IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.timelevelCode.mapOfGDgdjs_46timelevelCode_46GDbackObjects2Objects, runtimeScene, true, false);
if( gdjs.timelevelCode.condition0IsTrue_1.val ) {
    gdjs.timelevelCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.timelevelCode.GDbackObjects2.length;j<jLen;++j) {
        if ( gdjs.timelevelCode.GDbackObjects1_1final.indexOf(gdjs.timelevelCode.GDbackObjects2[j]) === -1 )
            gdjs.timelevelCode.GDbackObjects1_1final.push(gdjs.timelevelCode.GDbackObjects2[j]);
    }
}
}
{
gdjs.timelevelCode.condition1IsTrue_1.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if( gdjs.timelevelCode.condition1IsTrue_1.val ) {
    gdjs.timelevelCode.conditionTrue_1.val = true;
}
}
{
gdjs.copyArray(gdjs.timelevelCode.GDbackObjects1_1final, gdjs.timelevelCode.GDbackObjects1);
}
}
}if (gdjs.timelevelCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", false);
}}

}


{


gdjs.timelevelCode.condition0IsTrue_0.val = false;
{
gdjs.timelevelCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 300, "timerlevel");
}if (gdjs.timelevelCode.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(7).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "New scene", true);
}}

}


};

gdjs.timelevelCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.timelevelCode.GDbackObjects1.length = 0;
gdjs.timelevelCode.GDbackObjects2.length = 0;
gdjs.timelevelCode.GDtimetextObjects1.length = 0;
gdjs.timelevelCode.GDtimetextObjects2.length = 0;

gdjs.timelevelCode.eventsList0(runtimeScene);
return;

}

gdjs['timelevelCode'] = gdjs.timelevelCode;
