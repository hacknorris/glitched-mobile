gdjs.evtsExt__Micro__Loundness = {};

gdjs.evtsExt__Micro__Loundness.conditionTrue_0 = {val:false};
gdjs.evtsExt__Micro__Loundness.condition0IsTrue_0 = {val:false};


gdjs.evtsExt__Micro__Loundness.userFunc0x6c62b8 = function(runtimeScene, eventsFunctionContext) {
"use strict";
if(gdjs.microphone.meter) {
    eventsFunctionContext.returnValue = gdjs.microphone.meter.volume;
}

};
gdjs.evtsExt__Micro__Loundness.eventsList0 = function(runtimeScene, eventsFunctionContext) {

{


gdjs.evtsExt__Micro__Loundness.userFunc0x6c62b8(runtimeScene, typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined);

}


};

gdjs.evtsExt__Micro__Loundness.func = function(runtimeScene, parentEventsFunctionContext) {
var eventsFunctionContext = {
  _objectsMap: {
},
  _objectArraysMap: {
},
  _behaviorNamesMap: {
},
  getObjects: function(objectName) {
    return eventsFunctionContext._objectArraysMap[objectName] || [];
  },
  getObjectsLists: function(objectName) {
    return eventsFunctionContext._objectsMap[objectName] || null;
  },
  getBehaviorName: function(behaviorName) {
    return eventsFunctionContext._behaviorNamesMap[behaviorName];
  },
  createObject: function(objectName) {
    var objectsList = eventsFunctionContext._objectsMap[objectName];
    if (objectsList) {
      return parentEventsFunctionContext ?
        parentEventsFunctionContext.createObject(objectsList.firstKey()) :
        runtimeScene.createObject(objectsList.firstKey());
    }
    return null;
  },
  getArgument: function(argName) {
    return "";
  },
  getOnceTriggers: function() { return runtimeScene.getOnceTriggers(); }
};


gdjs.evtsExt__Micro__Loundness.eventsList0(runtimeScene, eventsFunctionContext);
return Number(eventsFunctionContext.returnValue) || 0;
}

